" tabs, spaces, linebreaks
au BufNewFile,BufRead *.tex setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=0 expandtab autoindent fileformat=unix wrap linebreak nolist