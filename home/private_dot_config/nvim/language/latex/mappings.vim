"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: LaTeX specifics
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" changes package accept
function! AcceptChanges()
    " Hover over '\replaced', '\added' or '\deleted' from the changes package and
    " remove it
    "E.g. \replaced{New}{Old} -> New
    "E.g. \replaced[comment=asdf]{New}{Old} -> New
    "E.g. \added{New} -> New
    "E.g. \deleted[id=3]{Old} -> 
    "E.g. Conti\replaced[comment=asdf]{nuing}{uing} -> Continuing
    "E.g. \replaced[comment={asdf}]{New}{Old} -> New
    "E.g. \replaced[comment={asdf my strange [] {}}]{New}{Old} -> New
    "E.g. \replaced[%
    "       comment={%
    "           asdf my strange [] {}
    "           }
    "     ]{%
    "       New
    "     }{%
    "     Old
    "     } -> New
    "
    "
    "TODO:
    let word = expand('<cword>')
    let WORD = expand('<cWORD>')
    if (word == 'replaced') || (word == 'added') || (word == 'deleted') || (word == 'highlight') || (word == 'comment') || (word == 'TODO')
        " " go one char right to be sure to catch the \
        " normal l
        " " search backwards to \
        " normal ?\\\\
        " delete till {}
        if stridx(WORD, '[') >= 0 && ((stridx(WORD, '[') < stridx(WORD, '{')) || stridx(WORD, '{') < 0)
            echo "[ IN THERE ]"
            normal dt[
            normal d%
            normal iX
        endif
        normal dt{
        if (word == 'replaced') || (word == 'added') || (word == 'highlight') || (word == 'comment')
            " Save cursor position
            let l:save_cursor = getpos(".") 
            " Go to matching bracket
            normal %
            " Delete bracket
            normal dl
            if (word == 'replaced')
                " Delete Old part
                normal d%
            endif
            " Restore cursor position
            call setpos('.', l:save_cursor)
            " Delete bracket before New
            normal dl
        else 
            " Delete Old part
            normal d%
        endif
    endif
endfunction

" TODO: put to which-key
autocmd FileType tex,latex nnoremap <Leader>y :call AcceptChanges()<CR>

autocmd FileType tex,latex noremap <Leader>n /\\replaced\\|\\added\\|\\deleted<CR>
autocmd FileType tex,latex noremap <Leader>N ?\\replaced\\|\\added\\|\\deleted<CR>
autocmd FileType tex,latex noremap <Leader>a i\added[id=DB,comment=<++>]{<++>}
autocmd FileType tex,latex noremap <Leader>A i\added[id=DB]{<++>}
autocmd FileType tex,latex inoremap <C-a> \added[id=DB,comment=<++>]{<++>}
autocmd FileType tex,latex inoremap <C-A> \added[id=DB]{<++>}
autocmd FileType tex,latex noremap <Leader>t i\TODO{DB: <++>}
autocmd FileType tex,latex xmap <Leader>d S}i\deleted[id=DB,comment=<++>]
autocmd FileType tex,latex xmap <Leader>D S}i\deleted[id=DB]
autocmd FileType tex,latex xmap <Leader>h S}i\highlight[id=DB,comment=<++>]
autocmd FileType tex,latex xmap <Leader>H S}i\highlight[id=DB]
autocmd FileType tex,latex xmap <Leader>c S}i\comment[id=DB]
autocmd FileType tex,latex xmap <Leader>r S}i\replaced[id=DB,comment=<++>]{<++>}
autocmd FileType tex,latex xmap <Leader>R S}i\replaced[id=DB]{<++>}
