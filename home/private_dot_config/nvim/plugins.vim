"""""""""""""
" INFO: 
" " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " "
" For the use with vscode, the following vscode extensions are required/optional
"   Required:
"       * 'VSCode Neovim'
"   Optionals 
"       * 'LaTeX-Workshop': required for which-key
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-plug path
let plug_path = expand('~/.local/share/nvim/site/autoload/plug.vim')

let plug_directory = expand('~/.local/share/nvim/plugged')
" Plugin directory
let plug_directory = expand('~/.local/share/nvim/plugged')

" Plugin config directory
let plug_config_directory = expand('~/.config/nvim/plug-config')

" Dictionary of plugins
let plugins = {}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Motion / Shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Eeasy motion by searching occurence of characters. Similar plugin is vim-sneak
" s <char><char> search
" let plugins['vscode-easymotion'] = {
"     \ 'source': 'ChristianChiarulli/vscode-easymotion',
"     \ 'if': 'vscode',
" \ }
let plugins['easymotion'] = {
    \ 'source': 'easymotion/vim-easymotion',
    \ 'if': 'vim',
\ }
let plugins['hop'] = {
    \ 'source': 'phaazon/hop.nvim',
    \ 'if': 'neovim,vscode',
\ }
let plugins['leap'] = {
    \ 'source': 'ggandor/leap.nvim',
    \ 'if': 'neovim,vscode',
\ }

" moving across a line with f, F, t, T
let plugins['quickscope'] = {
    \ 'source': 'unblevable/quick-scope',
    \ 'if': 'vim,neovim,vscode',
    \ }

" which key - we use the vscode plugin for vscode
let plugins['which-key'] = {
    \ 'source': 'liuchengxu/vim-which-key',
    \ 'if': 'vim,neovim',
    \ }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Utility
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""
" Building
"""""""""""""""""""""""""""""""""""""
" Asynchronous make (:Neomake)
let plugins['neomake'] = {
    \ 'source': 'neomake/neomake',
    \ 'if': 'vim,neovim'
\ }
" General asynchronous processes
let plugins['asyncrun.vim'] = {
    \ 'source': 'skywind3000/asyncrun.vim',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Knolwedge Base
"""""""""""""""""""""""""""""""""""""
" TODO: Consider vim-wiki

"""""""""""""""""""""""""""""""""""""
" Git
"""""""""""""""""""""""""""""""""""""
" If you know how to use Git at the command line, you know how to use :Git.
" It's vaguely akin to :!git but with numerous improvements
" TODO-0: add to vscode and use it in which-key
" TODO: sort out which-key
let plugins['vim-fugitive'] = {
    \ 'source': 'tpope/vim-fugitive',
    \ 'if': 'vim,neovim',
    \ }
" wrapper around git log --graph
let plugins['gitv'] = {
    \ 'source': 'gregsexton/gitv',
    \ 'if': 'vim,neovim',
\ }
" Show a git diff in the gutter
" Nice docu on https://vimawesome.com/plugin/vim-gitgutter
let plugins['gitgutter'] = {
    \ 'source': 'airblade/vim-gitgutter',
    \ 'if': 'vim,neovim',
\ }
let plugins['git-messenger'] = {
    \ 'source': 'rhysd/git-messenger.vim',
    \ 'if': 'vim,neovim',
\ }
" Git
let plugins['vim-rhubarb'] = {
    \ 'source': 'tpope/vim-rhubarb',
    \ 'if': 'vim,neovim'
\ }
let plugins['gv.vim'] = {
    \ 'source': 'junegunn/gv.vim',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Gists 
"""""""""""""""""""""""""""""""""""""
" Easily Create Gists
let plugins['vim-gist'] = {
    \ 'source': 'mattn/vim-gist',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Web api
"""""""""""""""""""""""""""""""""""""
let plugins['webapi-vim'] = {
    \ 'source': 'mattn/webapi-vim',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Browser
"""""""""""""""""""""""""""""""""""""
" Neovim in Browser
" TODO: Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(1) } }
"  let plugins['firenvim'] = {
"      \ 'source': 'glacambre/firenvim', { 'do': { _ -> firenvim#install(1) } },
"      \ 'if': 'vim,neovim'
"  \ }

"""""""""""""""""""""""""""""""""""""
" Terminal
"""""""""""""""""""""""""""""""""""""
" terminal support, managing multiple terminals, terminal window style, ...
let plugins['floaterm'] = {
    \ 'source': 'voldikss/vim-floaterm',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Interactive code
"""""""""""""""""""""""""""""""""""""
let plugins['codi'] = {
    \ 'source': 'metakirby5/codi.vim',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Debugging
"""""""""""""""""""""""""""""""""""""
" Todo: consider vimspector
" " Plugin 'SkyLeach/pudb.vim'
" " Installation:
" "     * Follow this https://puremourning.github.io/vimspector-web/demo-setup.html
" "     * The addition to the vimrc is obviously done already
" let g:vimspector_enable_mappings = 'HUMAN'
" packadd! vimspector

" Debugging
" https://github.com/serebrov/vimrc/blob/b8c2349ebb1eedd294ab0b65ecc4367e3a4df3c7/.vimrc#L1543
" The above can be a source of inspiration for debug dialects
" Installation: pip3 install komodo-python3-dbgp
let plugins['vdebug'] = {
    \ 'source': 'vim-vdebug/vdebug',
    \ 'options': {'on': 'VdebugStart'},
    \ 'if': 'vim,neovim'
\ }

" TODO: google/vim-maktaba

"""""""""""""""""""""""""""""""""""""
" Testing
"""""""""""""""""""""""""""""""""""""
" :TestNearest / :TestFile
let plugins['vim-test'] = {
    \ 'source': 'janko-m/vim-test',
    \ 'if': 'vim,neovim'
\ }
" Test.vim can run tests using different execution environments ("strategies")
let test#strategy = "neomake"
" mappings:
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Text / Editing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Use <C-A>/<C-X> to increment dates :SpeedDatinFormat default is YYYY-MM-DD
let plugins['vim-speeddating'] = {
    \ 'source': 'tpope/vim-speeddating',
    \ 'if': 'vim,neovim,vscode',
\ }
" TODO debug lua
"  let plugins['dial'] = {
"      \ 'source': 'monaqa/dial.nvim',
"      \ 'if': 'neovim,vscode',
"      \ }

"""""""""""""""""""""""""""""""""""""
" snippets
"""""""""""""""""""""""""""""""""""""
" Snippet engine.
" Good documentation here: https://github.com/SirVer/ultisnips/blob/master/doc/UltiSnips.txt
" TODO: ultisnips reqires python
"  let plugins['ultisnips'] = {
"      \ 'source': 'SirVer/ultisnips',
"      \ 'if': 'vim,neovim',
"      \ }
" Snippets are separated from the engine. Add this if you want them:
let plugins['vim-snippets'] = {
    \ 'source': 'honza/vim-snippets',
    \ 'if': 'vim,neovim',
\ }
let plugins['emmet-vim'] = {
    \ 'source': 'mattn/emmet-vim',
    \ 'if': 'vim,neovim',
\ }
"  " TODO: Maybe add mustache?
"  " Plugin 'mustache/vim-mustache-handlebars'
"  " or
"  " Plugin 'tobyS/vmustache

" mappings to easily delete, change and add surroundings (parentheses,
" brackets, quotes, XML tags, and more) in pairs
" E.g. Press ysiw] to add [] around a word
" E.g. Visually select stuff, press S) to add () around a word
" Note: I decided to delete auto-close since it is anooying me more than it helps.
"  let plugins['vim-surround'] = {
"      \ 'source': 'tpope/vim-surround',
"      \ 'if': 'vim,neovim',
"  \ }

"""""""""""""""""""""""""""""""""""""
" splitting lines
"""""""""""""""""""""""""""""""""""""
" Press
"    gS to split a one-liner into multiple lines
"    gJ (with the cursor on the first line of a block)
"          to join a block into a single-line statement.
let plugins['splitjoin'] = {
    \ 'source': 'AndrewRadev/splitjoin.vim',
    \ 'if': 'vim,neovim,vscode',
    \ }

"""""""""""""""""""""""""""""""""""""
" Commenting
"""""""""""""""""""""""""""""""""""""
" Toggle comments with gcc or gc<movement>
let plugins['vim-commentary'] = {
    \ 'source': 'tpope/vim-commentary',
    \ 'if': 'vim,neovim',
    \ }

" TODO: unify with vim-commentary
" Easy commenting of code for many filetypes
let plugins['nerd-commenter'] = {
    \ 'source': 'preservim/nerdcommenter',
    \ 'if': 'vim,neovim,vscode',
    \ }

" Automatically closing breakets
" TODO lvim used 'jiangmiao/auto-pairs'
let plugins['vim-autoclose'] = {
    \ 'source': 'Townk/vim-autoclose',
    \ 'if': 'vim,neovim',
    \ }

" line up text with the :Tabularize /<alignment character>
" You do not need to specify a range or visually select thinigs
" For options like alignment look at :help Tabularize
let plugins['tabular'] = {
    \ 'source': 'godlygeek/tabular',
    \ 'if': 'vim,neovim',
    \ }

"""""""""""""""""""""""""""""""""""""
" Formatting
"""""""""""""""""""""""""""""""""""""
" Format code with formatter on save (e.g. black for python)
" Installation: Requires black to be installed for python: pip3 install black
let plugins['vim-autoformat'] = {
    \ 'source': 'chiel92/vim-autoformat',
    \ 'if': 'vim,neovim,vscode'
\ }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Appearance
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" theme
let plugins['theme-nvcode'] = {
    \ 'source': 'christianchiarulli/nvcode-color-schemes.vim',
    \ 'if': 'vim,neovim',
\ }

" icons
let plugins['nvim-web-devicons'] = {
    \ 'source': 'kyazdani42/nvim-web-devicons',
    \ 'if': 'neovim',
\ }
let plugins['vim-devicons'] = {
    \ 'source': 'ryanoasis/vim-devicons',
    \ 'if': 'vim,neovim',
\ }

let plugins['lightbulg'] = {
    \ 'source': 'kosayoda/nvim-lightbulb',
    \ 'if': 'neovim',
\ }

" status line
" statusline at the bottom of each window
"  let plugins['vim-airline'] = {
"      \ 'source': 'vim-airline/vim-airline'
"      \ 'if': 'vim',
"  \ }
"  let plugins['vim-airline-themes'] = {
"      \ 'source': 'vim-airline/vim-airline-themes',
"      \ 'if': 'vim',
"  \ }
let plugins['galaxyline.nvim'] = {
    \ 'source': 'nvimdev/galaxyline.nvim',
    \ 'if': 'vim,neovim'
\ }

" Ranger
let plugins['rnvimr'] = {
    \ 'source': 'kevinhwang91/rnvimr',
    \ 'if': 'neovim'
\ }

" Start Screen
let plugins['start-screen'] = {
    \ 'source': 'mhinz/vim-startify',
    \ 'if': 'vim,neovim'
\ }

" Vista
let plugins['vista'] = {
    \ 'source': 'liuchengxu/vista.vim',
    \ 'if': 'vim,neovim'
\ }

" Zen mode
let plugins['goyo'] = {
    \ 'source': 'junegunn/goyo.vim',
    \ 'if': 'vim,neovim'
\ }

" TODO: requires code-minimap installed
"  " minimap
"  let plugins['minimap'] = {
"      \ 'source': 'wfxr/minimap.vim',
"      \ 'if': 'vim,neovim',
"      \ }

" Better tabline
let plugins['barbar'] = {
    \ 'source': 'romgrk/barbar.nvim',
    \ 'if': 'vim,neovim'
\ }
"  " Numbers beside tabs
"  let plugins['xtabline'] = {
"      \ 'source': 'mg979/vim-xtabline',
"      \ 'if': 'vim,neovim'
"  \ }

" Smooth scroll
let plugins['vim-smoothie'] = {
    \ 'source': 'psliwka/vim-smoothie',
    \ 'if': 'vim,neovim'
\ }

" Highlighting other uses of the word under cursor
let plugins['illuminate'] = {
    \ 'source': 'RRethy/vim-illuminate',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" File System
"""""""""""""""""""""""""""""""""""""
" Have the file system follow you around
let plugins['vim-rooter'] = {
    \ 'source': 'airblade/vim-rooter',
    \ 'if': 'vim,neovim',
\ }

" Find and replace
let plugins['far'] = {
    \ 'source': 'brooth/far.vim',
    \ 'if': 'vim,neovim'
\ }

" Telescope
let plugins['popup.nvim'] = {
    \ 'source': 'nvim-lua/popup.nvim',
    \ 'if': 'vim,neovim'
\ }
let plugins['plenary.nvim'] = {
    \ 'source': 'nvim-lua/plenary.nvim',
    \ 'if': 'vim,neovim'
\ }
let plugins['telescope.nvim'] = {
    \ 'source': 'nvim-telescope/telescope.nvim',
    \ 'if': 'vim,neovim'
\ }
let plugins['telescope-media-files.nvim'] = {
    \ 'source': 'nvim-telescope/telescope-media-files.nvim',
    \ 'if': 'vim,neovim'
\ }

""""""""""""""""""""
" Tree File Browser
""""""""""""""""""""
" Neovim
let plugins['nvim-tree'] = {
    \ 'source': 'kyazdani42/nvim-tree.lua',
    \ 'if': 'neovim'
\ }
" Vim
let plugins['nerdtree'] = {
    \ 'source': 'scrooloose/nerdtree',
    \ 'if': 'vim'
\ }

" Ranger integration
let plugins['ranger'] = {
    \ 'source': 'francoiscabrol/ranger.vim',
    \ 'if': 'vim,neovim,vscode',
    \ }

""""""""""""""""""""
" Fast file opening with fuzzy find
""""""""""""""""""""
let plugins['fzf'] = {
    \ 'source': 'junegunn/fzf',
    \ 'options': {
        \ 'dir': '~/.fzf',
        \ 'do': './install --all'
    \ },
    \ 'if': 'vim,neovim'
\ }
let plugins['fzf.vim'] = {
    \ 'source': 'junegunn/fzf.vim',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Languages
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""
" Better Syntax Support
""""""""""""""""""""
let plugins['polyglot'] = {
    \ 'source': 'sheerun/vim-polyglot',
    \ 'if': 'vim,neovim'
\ }

""""""""""""""""""""
" Linter
""""""""""""""""""""
" Set flake8 and pylint as code checker
" Installation: pip3 install flake8
" Installation: pip3 install pylint
let plugins['ale'] = {
    \ 'source': 'dense-analysis/ale',
    \ 'if': 'vim,neovim'
\ }
" TODO: once you are satisfied with linting: remove
"  """""""""""""""""""""""""""""""""""""
"  " Syntax checkers
"  """""""""""""""""""""""""""""""""""""
"  " Plugin 'vim-syntastic/syntastic'
"  " " Syntastic recommended Configuration
"  " set statusline+=%#warningmsg#
"  " set statusline+=%{SyntasticStatuslineFlag()}
"  " set statusline+=%*
"  " 
"  " let g:syntastic_always_populate_loc_list = 1
"  " let g:syntastic_auto_loc_list = 1
"  " let g:syntastic_check_on_open = 1
"  " " let g:syntastic_check_on_wq = 0
"  " let g:syntastic_python_checkers = "['flake8', 'pylint'] pylint is rather slow
"  " let g:syntastic_python_flake8_exec = 'python3'
"  " let g:syntastic_python_flake8_args = ['-m', 'flake8']
"  " 
"  " let g:syntastic_error_symbol = '❌'
"  " let g:syntastic_style_error_symbol = '⁉️'
"  " let g:syntastic_warning_symbol = '⚠️'
"  " let g:syntastic_style_warning_symbol = '💩'



"""""""""""""""""""""""""""""""""""""
" Natural
"""""""""""""""""""""""""""""""""""""
" Style and grammar checker
" TODO: handle auto-install via chezmoi
let plugins['languagetool'] = {
    \ 'source': 'dpelle/vim-LanguageTool',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Python
"""""""""""""""""""""""""""""""""""""
" converts Vim into a Python IDE
" Support Python and 3.6+
" Syntax highlighting
" Virtualenv support
" Run python code (<leader>r)
" Add/remove breakpoints (<leader>b)
" Improved Python indentation
" Python motions and operators (]], 3[[, ]]M, vaC, viM, daC, ciM, ...)
" Improved Python folding
" Run multiple code checkers simultaneously (:PymodeLint)
" Autofix PEP8 errors (:PymodeLintAuto)
" Search in python documentation (<leader>K)
" Code refactoring
" Intellisense code-completion
" Go to definition (<C-c>g)
let plugins['python-mode'] = {
    \ 'source': 'python-mode/python-mode',
    \ 'if': 'vim,neovim'
\ }
" autoindent will help, but in some cases (like when a function signature spans
" multiple lines), it doesn’t always do what you want, especially when it comes
" to conforming to PEP 8 standards. To fix that, you can use the
" indentpython.vim extension
let plugins['indentpython.vim'] = {
    \ 'source': 'vim-scripts/indentpython.vim',
    \ 'if': 'vim,neovim'
\ }


"""""""""""""""""""""""""""""""""""""
" Markdown/Text specifica
"""""""""""""""""""""""""""""""""""""
" Markdown Preview
let plugins['markdown-preview'] = {
    \ 'source': 'iamcco/markdown-preview.nvim',
    \ 'if': 'vim,neovim'
\ }

" Syntax highlighting, matching rules and mappings for the original Markdown
" and extensions.
let plugins['vim-markdown'] = {
    \ 'source': 'plasticboy/vim-markdown',
    \ 'if': 'vim,neovim'
\ }

" recognize github code blocks
let plugins['vim-flavored-markdown'] = {
    \ 'source': 'jtratner/vim-flavored-markdown',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" Webdev
"""""""""""""""""""""""""""""""""""""
" Auto change html tags
let plugins['tagalong'] = {
    \ 'source': 'AndrewRadev/tagalong.vim',
    \ 'if': 'vim,neovim'
\ }
" Closetags
let plugins['vim-closetag'] = {
    \ 'source': 'alvan/vim-closetag',
    \ 'if': 'vim,neovim'
\ }
" Colorizer
let plugins['nvim-colorizer.lua'] = {
    \ 'source': 'norcalli/nvim-colorizer.lua',
    \ 'if': 'vim,neovim'
\ }
" live server
let plugins['bracey'] = {
    \ 'source': 'turbio/bracey.vim',
    \ 'if': 'vim,neovim'
\ }
" Better quickfix
let plugins['nvim-bqf'] = {
    \ 'source': 'kevinhwang91/nvim-bqf',
    \ 'if': 'vim,neovim'
\ }

"""""""""""""""""""""""""""""""""""""
" LaTeX
"""""""""""""""""""""""""""""""""""""
" vim-latexsuite
" This is the origin of the <Ctrl-J> command
" F5 -> environment manager
" F9 in between \ref{} / \cite{} / includegraphics brackets -> list of options
"
" Normal-Mode-Shortcuts: 
" `^   expands to   \Hat{<++>}<++>
" `_   expands to   \bar{<++>}<++>
" `6   expands to   \partial
" `8   expands to   \infty
" `/   expands to   \frac{<++>}{<++>}<++>
" `%   expands to   \frac{<++>}{<++>}<++>
" `@   expands to   \circ
" `0   expands to   ^\circ
" `=   expands to   \equiv
" `\   expands to   \setminus
" `.   expands to   \cdot
" `*   expands to   \times
" `&   expands to   \wedge
" `-   expands to   \bigcap
" `+   expands to   \bigcup
" `(   expands to   \subset
" `)   expands to   \supset
" `<   expands to   \le
" `>   expands to   \ge
" `,   expands to   \nonumber
" `~   expands to   \tilde{<++>}<++>
" `;   expands to   \dot{<++>}<++>
" `:   expands to   \ddot{<++>}<++>
" `2   expands to   \sqrt{<++>}<++>
" `|   expands to   \Big|
" `I   expands to   \int_{<++>}^{<++>}<++>
"
" Visual-Mode-Shortcuts: 
" `(  encloses selection in \left( and \right)
" `[  encloses selection in \left[ and \right]
" `{  encloses selection in \left\{ and \right\}
" `$  encloses selection in $$ or \[ \] depending on characterwise or linewise selection
let plugins['vim-latex-suite'] = {
    \ 'source': 'gerw/vim-latex-suite',
    \ 'if': 'vim,neovim',
    \ }

"  " live Preview
"  TODO: check and add to whichkey instead of F6
"  let plugins['vim-latex-live-preview'] = {
"      \ 'source': 'xuhdev/vim-latex-live-preview',
"      \ 'if': 'vim,neovim',
"      \ }

"""""""""""""""""""""""""""""""""""""
" Inttelisense
"""""""""""""""""""""""""""""""""""""
" Treesitter
let plugins['nvim-treesitter'] = {
    \ 'source': 'nvim-treesitter/nvim-treesitter',
    \ 'if': 'vim,neovim',
\ }
let plugins['playground'] = {
    \ 'source': 'nvim-treesitter/playground',
    \ 'if': 'vim,neovim',
\ }
let plugins['rainbow'] = {
    \ 'source': 'p00f/nvim-ts-rainbow',
    \ 'if': 'vim,neovim',
\ }

" Tagbar
" Quick tag browser for the current file, a must have if you are using any
" kind of ctags like exuberant-tags.
let plugins['tagbar'] = {
    \ 'source': 'majutsushi/tagbar',
    \ 'if': 'vim,neovim'
\ }

" Intellisense
let plugins['lsp-config'] = {
    \ 'source': 'neovim/nvim-lspconfig',
    \ 'if': 'vim,neovim'
\ }
let plugins['nvim-compe'] = {
    \ 'source': 'hrsh7th/nvim-compe',
    \ 'if': 'vim,neovim'
\ }
let plugins['lsp-saga'] = {
    \ 'source': 'glepnir/lspsaga.nvim',
    \ 'if': 'vim,neovim'
\ }
let plugins['lspkind-nvim'] = {
    \ 'source': 'onsails/lspkind-nvim',
    \ 'if': 'vim,neovim'
\ }
let plugins['nvim-lightbulb'] = {
    \ 'source': 'kosayoda/nvim-lightbulb',
    \ 'if': 'vim,neovim'
\ }
let plugins['nvim-jdtls'] = {
    \ 'source': 'mfussenegger/nvim-jdtls',
    \ 'if': 'vim,neovim'
\ }
let plugins['nvim-dap'] = {
    \ 'source': 'mfussenegger/nvim-dap',
    \ 'if': 'vim,neovim'
\ }

" TODO check if required and compare with above
"  " Code completion
"  " Use <tab> and <Shift>-<tab> to cycle through proposed completions
"  Plugin 'ycm-core/YouCompleteMe'
"  " Automatically closing popups again
"  let g:ycm_autoclose_preview_window_after_insertion = 1
"  let g:ycm_autoclose_preview_window_after_completion = 1
"  map <C-n> :NERDTreeToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Automatic plugin handling
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" auto-install vim-plug. See https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation
if empty(glob(plug_path))
    echomsg 'plug.vim is missing. Installing ...'
    silent execute '!curl -fLo '.plug_path.' --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Loop over dictionary and install plugins
call plug#begin(plug_directory)
for [name, properties] in items(plugins)
    let envs = split(properties['if'], ',')
    let load_plugin = empty(properties['if'])
    for env in envs
        if env == 'vim' && has('vim8') && !(exists('g:vscode'))
            let load_plugin = 1
            break
        elseif env == 'neovim' && has('nvim') && !(exists('g:vscode'))
            let load_plugin = 1
            break
        elseif env == 'vscode' && exists('g:vscode')
            let load_plugin = 1
            break
        endif
    endfor
    let plugins[name].load_plugin = load_plugin

    if load_plugin
        let plugin_options = get(properties, 'options', {})
        if len(plugin_options) > 0
            " execute 'Plug ' . properties['source'] . ' ,' . string(plugin_options)
            Plug properties['source'], plugin_options
        else
            Plug properties['source']
        endif
    endif
endfor
call plug#end()

" Loop over dictionary again and load plugin settings files.
" This is done after loading all plugins, specifically because themes have to be loaded after plug#end
let valid_extensions = ['vim', 'lua']
for [name, properties] in items(plugins)
    if plugins[name].load_plugin
        " Load plugin settings file, if specified
        let settings_files = split(glob(plug_config_directory . '/' . name . '.*'))
        if len(settings_files) > 1
            echoerr 'Warning: multiple settings files found for plugin "' . name . " namely " . string(settings_files)
        endif
        let settings_file = ""
        for file in settings_files
            if file =~ '\.vim$'
                let settings_file = file
                break
            elseif file =~ '\.lua$'
                let settings_file = file
                break
            endif
        endfor
        if settings_file != ""
            let settings_language = substitute(settings_file, '.*\.\(\w\+\)$', '\1', '')
            if settings_language == 'lua' && filereadable(settings_file)
                execute 'luafile ' . settings_file
            elseif filereadable(settings_file)
                execute 'source ' . settings_file
            endif
        endif
    endif
endfor

" Automatically install missing plugins on startup
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
