let g:highlightedyank_highlight_duration = 500

function! s:openWhichKeyInVisualMode()
    normal! gv
    let visualmode = visualmode()
    if visualmode == "V"
        let startLine = line("v")
        let endLine = line(".")
        " call VSCodeNotifyRange("whichkey.show", startLine, endLine, 1)
        call VSCodeNotifyRange("vspacecode.space", startLine, endLine, 1)
    else
        let startPos = getpos("v")
        let endPos = getpos(".")
        " call VSCodeNotifyRangePos("whichkey.show", startPos[1], endPos[1], startPos[2], endPos[2], 1)
        call VSCodeNotifyRangePos("vspacecode.space", startPos[1], endPos[1], startPos[2], endPos[2], 1)
    endif
endfunction

" which-key view
nnoremap <silent> <Space> :call VSCodeNotify('vspacecode.space')<CR>
xnoremap <silent> <Space> :<C-u>call <SID>openWhichKeyInVisualMode()<CR>

function! s:add_ignore_problem_to_comment(problem, comment_)
    if a:problem.source =~ 'Pylint'
        " Map from code to message: https://gist.github.com/omniproc/965bb613177dd4fa896b815aa0e0e365
        let pattern = '^.*pylint: disable=\zs\S*'
        let format_string = 'pylint: disable=%s'
    elseif a:problem.source =~ 'Flake8'
        let pattern = '^.noqa:\zs\S*'
        let format_string = 'noqa:%s'
    elseif a:problem.source =~ 'Pylance'
        return a:comment_ . 'type: ignore[mypy] ' . a:problem.message
    else
        return a:comment_ . 'NOT-IMPLEMENTED-' . a:problem.source . '  problem: ' . a:problem.message . ' (' . a:problem.code . ')'
    endif

    echomsg "add_ignore_problem"
    echomsg pattern
    echomsg format_string
    let match_pos = match(a:comment_, pattern)
    if match_pos == -1
        let new_comment = printf(format_string, a:problem.code)
        echomsg new_comment
        return new_comment
    endif
    let pattern_length = len(matchstr(a:comment_, pattern))
    let current_disables = a:comment_[match_pos : match_pos + pattern_length - 1]
    let value = printf(format_string, current_disables . ',' . a:problem.code)
    let new_comment = substitute(a:comment_, pattern, value, '')
    echomsg new_comment
    return new_comment
endfunction

function! s:modify_line_to_ignore_problem(problem)
    echomsg "modify_line_to_ignore_problem"
    echomsg a:problem

    " get the current line and substitute \n
    let current_line = getline('.')
    let new_line = substitute(current_line, '\n$', '', '')

    " split the new_line in two variables, 'content' and 'comment'
    let comment_string_vim_regex_escaped = escape(&commentstring, '^$.*?/\[]~')
    let line_regex = '\v^(.*)' . substitute(comment_string_vim_regex_escaped, '%s', '(.*)', '') . '$'
    let matches = matchlist(new_line, line_regex)
    if len(matches) > 0
        let content = matches[1]
        let comment_ = matches[2]
    else
        let content = new_line
        let comment_ = ''
    endif
    " remove the spaces from the right of the content
    let content = substitute(content, '\s\+$', '', '')

    echomsg "content/comment split"
    echomsg content
    echomsg comment_
    
    let new_comment = s:add_ignore_problem_to_comment(a:problem, comment_)

    let current_line = line('.')
    call setline(current_line, content . '  ' . printf(&commentstring, new_comment))
endfunction

" A function to add e.g. "  # pylint:disable=E401" at the end of the line
" if the current line contains a problem with the "pylint" type and the "401" code.
function! IgnoreProblem()
    " Get the current line number
    let currentLine = line('.')

    " Call the 'problems-copy.copyCurrentFileMessages' command to copy the current file's problems to the clipboard
    call VSCodeCall('problems-copy.copyCurrentFileMessages')

    " Get the current file's problems from the clipboard
    let clipboard_contents = @*

    " Parse the JSON data
    let problems = json_decode(clipboard_contents)

    " Filter the problems to only those occurring on the current line
    let current_line_problems = filter(problems, {_, v -> v.startLineNumber == currentLine && (v.startLineNumber == v.endLineNumber)})
    echomsg problems
    echomsg current_line_problems

    " Sort the problems by their range, in ascending order
    let sorted_problems = sort(current_line_problems, {a, b -> a.startColumn - b.startColumn})

    " Choose the problem
    if len(sorted_problems) == 0
        return
    elseif len(sorted_problems) == 1
        let problem = sorted_problems[0]
    else
        " If there are multiple problems on the current line, show a dropdown menu with options to ignore each problem
        let problem_options = []
        for i in range(len(sorted_problems))
            let problem = sorted_problems[i]
            let problem_desc = problem.source . ': ' . problem.message
            let option = printf('%d. %s\n', i+1, problem_desc)
            call add(problem_options, option)
        endfor
        let prompt_message = "Select a problem to ignore:\n" . join(problem_options, "\n\n")
        let selected_option = inputdialog(prompt_message)
        if selected_option == ''
            return
        endif
        let selected_index = str2nr(selected_option) - 1
        if selected_index < 0 || selected_index >= len(sorted_problems)
            echom "Invalid option selected"
            return
        endif
        let problem = sorted_problems[selected_index]
    endif

    call s:modify_line_to_ignore_problem(problem)
endfunction

nnoremap gr <Cmd>call VSCodeNotify('editor.action.goToReferences')<CR>
