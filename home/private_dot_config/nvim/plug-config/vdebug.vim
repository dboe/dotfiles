" TODO: install patch (see python section of :help vdebug)
" Do not stop on the first line, Stop on the first breakpoint
" Stop listening immediately after a debugging session has finished
let g:vdebug_keymap = {
            \    'run' : '',
            \    'run_to_cursor' : '<F9>',
            \    'step_over' : '<F2>',
            \    'step_into' : '<F3>',
            \    'step_out' : '<F4>',
            \    'close' : '',
            \    'detach' : '<F7>',
            \    'set_breakpoint' : '<F10>',
            \    'get_context' : '<F11>',
            \    'eval_under_cursor' : '<F12>',
            \    'eval_visual' : '<Leader>e'
            \}
" Start the server and start debugging
" TODO: loop the debugger / destroy the process.
nnoremap <F5> :VdebugStart<CR>:AsyncRun pydbgp %:p /<CR>
nnoremap <F6> :AsyncStop <CR>:python3 debugger.close()<CR>

