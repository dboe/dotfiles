let g:pymode_python = 'python3'
let g:pymode_rope = 0
let g:pymode_rope_completion = 0
let g:pymode_lint = 0
let g:pymode_doc = 0
let g:pymode_indent = 1
let g:pymode_options_max_line_length = 99
