" Install: See "Download Language Tool " at https://github.com/dpelle/vim-LanguageTool
"     You need to put the path to the commandline.java file here
:let g:languagetool_jar='$HOME/.local/share/nvim/LanguageTool-4.9/languagetool-commandline.jar'