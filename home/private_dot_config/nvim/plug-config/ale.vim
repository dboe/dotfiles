" TODO: check if I am running another language server. Then disable with the below:
" let g:ale_disable_lsp = 1
let g:ale_linters = {
\   'python': ['flake8', 'pylint'],
\}
" show codecheck errors as a list in a bottom split window
let g:ale_open_list = 1
