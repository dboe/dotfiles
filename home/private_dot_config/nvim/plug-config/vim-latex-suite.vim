" environment shortcuts:
let g:Tex_Env_itemize = "\\begin{itemize}\<CR>\\item <++>\<CR>\\end{itemize}\<CR><++>"
let g:Tex_Env_description = "\\begin{description}\<CR>\\item[<++>] <++>\<CR>\\end{description}\<CR><++>"
let g:Tex_PromptedEnvironments='equation,equation*,align,align*,itemize,enumerate,description,figure'