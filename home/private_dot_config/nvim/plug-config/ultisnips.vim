" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
" Use Ctrl+Enter to trigger snippet expansion and Jumping
" TODO: Make it work with tab, forward/backward go with ctrl-j/ctrl-k
let g:UltiSnipsExpandTrigger="<C-CR>"
if !exists("g:UltiSnipsJumpForwardTrigger")
    let g:UltiSnipsJumpForwardTrigger = "<C-CR>"
    let g:UltiSnipsJumpBackwardTrigger="<C-S-CR>"
endif
" :UltiSnipsEdit spliting the window
let g:UltiSnipsEditSplit="vertical"
