source ~/.config/nvim/general/settings.vim
source ~/.config/nvim/general/functions.vim
source ~/.config/nvim/general/mappings.vim

if exists('g:vscode')
    " VS Code extension
    source ~/.config/nvim/vscode/settings.vim
endif

" Source all language settings e.g. source ~/.config/nvim/language/latex/mappings.vim
let languages = expand('~/.config/nvim/language/*')
for folder in glob(languages)
    for file in glob(languages . '/' . folder . '/*.vim')
        if filereadable(file)
            execute 'source ' . file
        endif
    endfor
endfor

source ~/.config/nvim/plugins.vim
