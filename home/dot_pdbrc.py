# # Command line history:
# import readline
# histfile = ".pdb-pyhist"
# try:
#     readline.read_history_file(histfile)
# except IOError:
#     pass
# import atexit
# atexit.register(readline.write_history_file, histfile)
# del histfile
# readline.set_history_length(200)
#
# # # return to debugger after fatal exception (Python cookbook 14.5):
# def info(type, value, tb):
#     if hasattr(sys, 'ps1') or not sys.stderr.isatty():
#         sys.__excepthook__(type, value, tb)
#     import traceback, pdb
#     traceback.print_exception(type, value, tb)
#     print()
#     pdb.pm()
#
# sys.excepthook = info
#
# # From (on my machine) /usr/local/lib/python2.3/less user.py:
# import os
# HOME = os.curdir                        # Default
# if 'HOME' in os.environ:
#     HOME = os.environ['HOME']
# elif os.name == 'posix':
#     HOME = os.path.expanduser("~/")
# elif os.name == 'nt':                   # Contributed by Jeff Bauer
#     if 'HOMEPATH' in os.environ:
#         if 'HOMEDRIVE' in os.environ:
#             HOME = os.environ['HOMEDRIVE'] + os.environ['HOMEPATH']
#         else:
#             HOME = os.environ['HOMEPATH']
# # Make sure HOME always ends with a directory separator:
# HOME = os.path.realpath(HOME) + os.sep
#
# # Try to execute local file (if any) unless we are in the HOME dir.
# if HOME != os.path.realpath(os.curdir) + os.sep:
#     # Avoid recursively loading this file.
#     try:
#         _ALREADY_LOADED += 1
#     except NameError:
#         _ALREADY_LOADED = 1
#     if _ALREADY_LOADED < 3:
#         try:
#             exec("pdbrc.py")
#         except IOError:
#             pass
#
# # Cleanup any variables that could otherwise clutter up the namespace.
# try:
#     del atexit
#     del HOME
#     del info
#     del os
#     del readline
#     # careful here:
#     del _ALREADY_LOADED
# except NameError:
#     # Probably this is a second pdbrc that has been loaded.
#     pass
#
# print(".pdbrc.py finished")
